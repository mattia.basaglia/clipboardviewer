#include "clipboardviewer.h"
#include "ui_clipboardviewer.h"

#include <unordered_map>
#include <QClipboard>
#include <QMimeData>


class ClipboardViewer::Private : public Ui::ClipboardViewer
{
public:
    struct ModeWid
    {
        QListWidget* list = nullptr;
        QPlainTextEdit* view = nullptr;
    };
    struct ClipboardData
    {
        ModeWid widgets;
        const QMimeData* mime = nullptr;
        bool ok = false;
    };

    std::unordered_map<QClipboard::Mode, ModeWid> mode_wid;

    void setup(::ClipboardViewer* parent)
    {
        setupUi(parent);
        mode_wid.emplace(QClipboard::Clipboard, ModeWid{list_clipboard, view_clipboard});
        update(QClipboard::Clipboard);
    }

    ClipboardData get(QClipboard::Mode mode)
    {
        auto tabi = mode_wid.find(mode);
        auto mimedata = QGuiApplication::clipboard()->mimeData(mode);
        if ( !mimedata || tabi == mode_wid.end() )
            return {};
        return ClipboardData{tabi->second, mimedata, true};
    }

    void update(QClipboard::Mode mode)
    {
        auto data = get(mode);
        if ( !data.ok )
            return;

        data.widgets.list->clear();
        data.widgets.view->setPlainText("");
        for ( const auto& s : data.mime->formats() )
        {
            data.widgets.list->addItem(s);
        }

        if ( data.mime->hasImage() )
        {
            img_clipboard->setVisible(true);
            QPixmap pix = QPixmap::fromImage(qvariant_cast<QImage>(data.mime->imageData()));
            img_clipboard->setPixmap(pix);
        }
        else
        {
            img_clipboard->setVisible(false);
        }
    }

    void activated(QClipboard::Mode mode, QListWidgetItem* index)
    {
        auto data = get(mode);
        if ( !data.ok || !index )
            return;

        data.widgets.view->setPlainText(
            data.mime->data(index->text())
        );
    }
};

ClipboardViewer::ClipboardViewer(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Private)
{
    m_ui->setup(this);
    QClipboard *clipboard = QGuiApplication::clipboard();
    connect(clipboard, &QClipboard::changed, this, [this](QClipboard::Mode mode){
        m_ui->update(mode);
    });
    connect(m_ui->list_clipboard, &QListWidget::currentItemChanged, this, [this](QListWidgetItem *current){
        m_ui->activated(QClipboard::Clipboard, current);
    });
}

ClipboardViewer::~ClipboardViewer() = default;
