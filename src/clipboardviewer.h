#ifndef CLIPBOARDVIEWER_H
#define CLIPBOARDVIEWER_H

#include <QMainWindow>
#include <QScopedPointer>

class ClipboardViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit ClipboardViewer(QWidget *parent = nullptr);
    ~ClipboardViewer() override;

private:
    class Private;
    QScopedPointer<Private> m_ui;
};

#endif // CLIPBOARDVIEWER_H
